# TRAIN Project Summary

## _For the lastest information please refer to the [Fraunhofer TRAIN website](https://www.hci.iao.fraunhofer.de/de/identity-management/identity-und-accessmanagement/TRAIN_EN.html)_

For more information, please contact:
michael.kubach@iao.fraunhofer.de

## Introduction

### About 
The Team Identity Management of Fraunhofer IAO (not for profit research organization) supported by University Stuttgart IAT are active in a number of research communities in identity management, trust, privacy and related spheres. We are working in collaborative research projects funded by the EU (e.g. LIGHTest: www.lightest.eu, github.com/H2020LIGHTest), national public institutions and are advising NGOs and the industry and co-organize conferences such as the Open Identity Summit (openidentity.eu).

### TRAIN (TRust mAnagement INfrastructure): Trust Management Infrastructure Component for the ESSIF-Lab Architecture

Our component aims to extend the ESSIF-Framework through a flexible trust infrastructure that can be used to verify the trustworthiness of involved parties exchanging credentials. The trust layer enables actors using the ESSIF-Framework to verify the root of trust for issuers of credentials.
In addition, the component allows for the definition, consideration, and verification of Trust Schemes compliance (e.g. eIDAS including LoAs or other Trust Schemes that can also be application/industry-specific) of involved parties. It is not dependent on a hierarchical CA infrastructure.
The component builds on the infrastructure developed in the EU project LIGHTest (2016-2020, G.A. No. 700321). The trust layer is flexible, individual parties can define their own trust policies, manage and publish them.
TRAIN is fully in line with the open and decentral SSI approach and complements other approaches.

The trust management architecture that is made possible by TRAIN enables secure, trustable digital interactions. At the same time a classical hierarchical CA-type structure is avoided - so is fraud, chaos and the pure dominance of the economically strongest actors in the system.

Individuals or groups (industry organizations, NGOs, etc.) of validators can define for themselves the trust standards they require. Issuers can publish to what standards they comply. The system is open, but standards for trust are transparent, as the Trust Schemes and Lists can be published.

**Relation to the SSI Concept**

TRAIN adds a flexible trust layer to be used by verifiers to define their required level of trust. No central authority is established, everyone can issue certificates, but TRAIN facilitates individual trust decisions.
Trust standards such as Trust Schemes (eIDAS, Pan Canadian Trust Framework, but also self-defined Schemes and Policies) can be integrated. In the ESSIF architecture, TRAIN particularly addresses the publication, query and management of Verifier & Issuer Policies and preferences.


**Background and Licensing**

The TRAIN component builds upon work completed in the EU H2020 LIGHTest research project (www.lightest.eu). Components developed in LIGHTest are released under Apache 2.0 license. TRAIN will also be released under Apache 2.0 or a license preferred by ESSIF-Lab.

## Summary

### Business Problem

In a very basic SSI architecture we identify four main challenges to be addressed by TRAIN:
1. Credentials can basically be issued by anyone. This is an important part of the decentralized, open architecture, but can cause issues of trust and fraud if a trust layer is missing.
2. For some, sensitive applications (e.g. certain relying parties such as online shops) the ability to decide which credentials can be trusted (in a transparent, automated manner) is required.
3. Verification of credentials in regards to their authenticity and validity (e.g. if they have been revoked) is required.
4. Handling of certain standards for the security level (Level of Assurance) and verification of credentials in an automated manner are required to make the easy handling of the various credentials from different sources possible and 


### Technical Solution

TRAIN:
*  introduces a  trust infrastructure that allows to verify the trustworthiness of involved parties (in an electronic transaction), e.g. is the issuer trustworthy (is it a real bank or just a fake bank)
*  adds a Trust-Component to ESSIF-Framework, which enables for the verification credential issuers, as well as the definition, consideration and verification of eIDAS compliance (including LoAs) of involved parties
 
TRAIN will provide a decentralized framework for the publication and querying of trust information, conceptually comparable to OCSP. It makes use of the Internet Domain Name System (DNS) with its existing global infrastructure, organization, governance and security standards.

TRAIN is a software component. Information is published via the so called TSPA server. The chain of trust is verified using DNSSEC components and the verification of credentials is achieved through using Trust Policies. The basic technology has already been validated through pilots and prototypes (outside the SSI-context).

## Integration with the eSSIF-Lab Functional Architecture and Community
The TRAIN component API interface specifications can be subvidived in 

- verification of issuers of credentials using an Automatic Trust Verifier: **Verifier API**
- _publication of trust schemes and trust lists: **Publication API** - for the first MVP the Publication API is deemed out of scope_

The general approach of TRAIN is described in the following paper: https://dl.gi.de/handle/20.500.12116/36489 

An illustrative interoperability use case for the European Health Insurance Card (EHIC) has been realized and demonstrated with SICPA SA and Validated ID. It is described in the following paper: https://dl.gi.de/handle/20.500.12116/36490
